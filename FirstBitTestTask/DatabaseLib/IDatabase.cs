﻿using System;
using System.Collections.Generic;

namespace DatabaseLib
{
    public interface IDatabase
    {
        void CreateEntityTables(IEnumerable<string> tables);
        IEntity SelectById(string table, Guid id);
        void Delete(string table, Guid id, bool inTransaction);
        void Save(IEntity obj, bool inTransaction);
        void CommitTransaction();
        void RollbackTransaction();
    }

    public interface IEntity
    {
        Guid Id { get; set; }
        string Tablename { get; }
    }
}
