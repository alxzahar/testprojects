using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DatabaseLib
{
    [TestClass]
    public class DataBaseTestWrapperFunctionalTests
    {
        private IDatabase GetDataBase()
        {
            return new DataBaseTestWrapper();
        }

        private IEntity GetEntity(Guid id, string table)
        {
            return new Entity(id, table);
        }

        private bool IsEqual(IEntity expect, IEntity actual)
        {
            return Guid.Equals(expect.Id, actual.Id) && string.Equals(expect.Tablename, actual.Tablename);
        }

        [TestMethod]
        public void CreateSaveSelect_single()
        {
            //arrange
            bool expect;
            var db = GetDataBase();
            Guid id = Guid.NewGuid();
            IEntity expectEntity = GetEntity(id, "Deps");

            // act
            db.CreateEntityTables(new[] { "Deps" });
            db.Save(expectEntity, false);
            IEntity actualEntity = db.SelectById("Deps", id);
            expect = IsEqual(expectEntity, actualEntity);

            // assert
            Assert.IsTrue(expect);
        }

        [TestMethod]
        public void CreateSaveSelect_multi()
        {
            //arrange
            var db = GetDataBase();
            Guid[] id_array = new Guid[100];

            //act
            db.CreateEntityTables(new[] { "Deps" });

            for (int i = 0; i < id_array.Length; i++)
            {
                Guid id = Guid.NewGuid();
                id_array[i] = id;
                IEntity newEntity = GetEntity(id, "Deps");
                db.Save(newEntity, false);
            }

            for (int i = 0; i < id_array.Length; i++)
            {
                IEntity currentEntity = db.SelectById("Deps", id_array[i]);
                //assert
                if (currentEntity == null) Assert.IsNotNull(currentEntity);
            }
        }

        [TestMethod]
        public void CreateSaveDeleteSelect_single()
        {
            //arrange
            var db = GetDataBase();
            Guid id = Guid.NewGuid();
            IEntity newEntity = GetEntity(id, "Employees");

            // act
            db.CreateEntityTables(new[] { "Employees" });
            db.Save(newEntity, false);
            db.Delete("Employees", id, false);
            IEntity removedEntity = db.SelectById("Employees", id);

            // assert
            Assert.IsNull(removedEntity);
        }

        [TestMethod]
        public void CreateSaveDeleteSelect_multi()
        {
            //arrange
            var db = GetDataBase();
            Guid[] id_array = new Guid[100];

            //act
            db.CreateEntityTables(new[] { "Employees" });

            for (int i = 0; i < id_array.Length; i++)
            {
                Guid id = Guid.NewGuid();
                id_array[i] = id;
                IEntity newEntity = GetEntity(id, "Employees");
                db.Save(newEntity, false);
            }

            for (int i = 0; i < id_array.Length; i++)
            {
                db.Delete("Employees", id_array[i], false);
                IEntity removedEntity = db.SelectById("Employees", id_array[i]);
                if (removedEntity != null) Assert.IsNull(removedEntity);
            }
        }

        [TestMethod]
        public void CreateSaveInTransactionCommitSelect_single()
        {
            //arrange
            bool expect;
            var db = GetDataBase();
            Guid id = Guid.NewGuid();
            IEntity expectEntity = GetEntity(id, "Employees");

            //act
            db.CreateEntityTables(new[] { "Employees" });
            db.Save(expectEntity, true);
            db.CommitTransaction();
            IEntity actualEntity = db.SelectById("Employees", id);
            expect = IsEqual(expectEntity, actualEntity);

            //assert
            Assert.IsTrue(expect);
        }

        [TestMethod]
        public void CreateSaveInTransactionRollbackSelect_single()
        {
            //arrange
            var db = GetDataBase();
            Guid id = Guid.NewGuid();
            IEntity expectEntity = GetEntity(id, "Employees");

            //act
            db.CreateEntityTables(new[] { "Employees" });
            db.Save(expectEntity, true);
            db.RollbackTransaction();
            db.CommitTransaction();
            IEntity actualEntity = db.SelectById("Employees", id);

            //assert
            Assert.IsNull(actualEntity);
        }

        [TestMethod]
        public void CreateSaveInTransactionCommitSelect_multi()
        {
            //arrange
            var db = GetDataBase();
            Guid[] id_array = new Guid[100];

            //act
            db.CreateEntityTables(new[] { "Employees" });

            for (int i = 0; i < id_array.Length; i++)
            {
                Guid id = Guid.NewGuid();
                id_array[i] = id;
                IEntity newEntity = GetEntity(id, "Employees");
                db.Save(newEntity, true);
            }

            db.CommitTransaction();

            for (int i = 0; i < id_array.Length; i++)
            {
                IEntity currentEntity = db.SelectById("Employees", id_array[i]);
                if (currentEntity == null) Assert.IsNotNull(currentEntity);
            }
        }

        [TestMethod]
        public void CommitWithoutTablesInDataBaseAndCash()
        {
            var db = GetDataBase();
            db.CommitTransaction();
        }

        [TestMethod]
        public void RollbackWithoutTablesIn()
        {
            var db = GetDataBase();
            db.RollbackTransaction();
        }
    }
}
