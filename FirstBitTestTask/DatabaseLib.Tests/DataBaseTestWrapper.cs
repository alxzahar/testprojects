﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace DatabaseLib
{
    public class DataBaseTestWrapper : IDatabase
    {

        private readonly Dictionary<string, Dictionary<Guid, IEntity>> _dataBase = new Dictionary<string, Dictionary<Guid, IEntity>>();
        private readonly Dictionary<string, Dictionary<Guid, IEntity>> _transactionCache = new Dictionary<string, Dictionary<Guid, IEntity>>();

        public void CreateEntityTables(IEnumerable<string> tables)
        {
            foreach (var table in tables)
            {
                _dataBase[table] = new Dictionary<Guid, IEntity>();
            }
        }

        public void Delete(string table, Guid id, bool inTransaction)
        {
            if (!inTransaction)
            {
                _dataBase[table].Remove(id);
            }
            else
            {
                _transactionCache[table].Remove(id);
            }
        }

        public void Save(IEntity obj, bool inTransaction)
        {
            if (!inTransaction)
            {
                _dataBase[obj.Tablename][obj.Id] = obj;
            }
            else
            {
                if (!_transactionCache.ContainsKey(obj.Tablename))
                {
                    _transactionCache[obj.Tablename] = new Dictionary<Guid, IEntity>();
                }
                _transactionCache[obj.Tablename][obj.Id] = obj;
            }
        }

        public IEntity SelectById(string table, Guid id)
        {
            if (_dataBase[table].[id].ContainsValue(id))
            {
                return _dataBase[table][id];
            }
            else
            {
                return null;
            }
            //return _dataBase[table][id];
        }

        public void CommitTransaction()
        {
            foreach (var item in _transactionCache)
            {
                foreach (var entity in item.Value)
                {
                    Save(entity.Value, false);
                }
            }
            _transactionCache.Clear();
        }

        public void RollbackTransaction()
        {
            _transactionCache.Clear();
        }
    }

    public class Entity : IEntity
    {
        public Guid Id { get; set; }
        public string Tablename { get; private set; }

        public Entity(Guid id, string tablename)
        {
            this.Id = id;
            this.Tablename = tablename;
        }
    }
}
