﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DatabaseLib
{
    [TestClass]
    public class DataBaseTestWrapperInputParametersTest
    {
        private IDatabase GetDataBase()
        {
            return new DataBaseTestWrapper();
        }

        private IEntity GetEntity(Guid id, string table)
        {
            return new Entity(id, table);
        }

        [TestMethod]
        public void CreateWithNullAsParam()
        {
            var db = GetDataBase();
            db.CreateEntityTables(null);
        }

        [TestMethod]
        public void CreateWithEmptyStringAsParam()
        {
            var db = GetDataBase();
            db.CreateEntityTables(new[] { string.Empty });
        }

        [TestMethod]
        public void CreateWithBadStringAsParam()
        {
            var db = GetDataBase();
            string badString = "!@#$%^&*()";
            db.CreateEntityTables(new[] { badString });
        }

        [TestMethod]
        public void SaveWithNullAsParam() 
        {
            var db = GetDataBase();
            Guid id = Guid.NewGuid();
            IEntity expectEntity = null;
            db.CreateEntityTables(new[] { "Deps" });
            db.Save(expectEntity, false);
        }

        [TestMethod]
        public void SelectWithNullAsParam() 
        {
            var db = GetDataBase();
            db.CreateEntityTables(new[] { "Deps" });
            IEntity actualEntity = db.SelectById(null, Guid.NewGuid());
        }

        [TestMethod]
        public void DeleteWithNullAsParam() 
        {
            var db = GetDataBase();
            db.CreateEntityTables(new[] { "Employees" });
            db.Delete(null, Guid.NewGuid(), false);
        }

    }
}
